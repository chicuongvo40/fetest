import React from 'react';
import { format } from 'date-fns';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './CallReport.module.css';
import { useMutation } from '@tanstack/react-query';
import { formartDate, formatNumber } from '~/utils/functions';
import {
  Box,
  styled,
  Table,
  Button,
  Typography
} from "@mui/material";
import vi from 'date-fns/locale/vi';
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { useAxios } from '~/context/AxiosContex';
import { useState } from 'react';
import { DataGrid } from "@mui/x-data-grid";
import DataGridCustomToolbar from "~/components/Material/DataGridCustomToolbar/DataGridCustomToolbar";
import { blue, red } from '@mui/material/colors';
export default function HistoryCall() {
  const [callData, setCallData] = useState([]);
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 11 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(
    new Date(new Date().getTime() + 1 * 60 * 60 * 1000),
  );

  const { getMissCall } =
  useAxios();
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  function returnPhone(phone){
    return phone.slice(6,15);
  }
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return getMissCall(data).then((res) => {
        console.log(res);
        setCallData(res.data);
        console.log(callData);
        return res;
      });
    },
  });
  const handleAddUser = (e) => {
    e.preventDefault();
    const data = {
      dateStart: format(startDate.toISOString(),'yyyy-MM-dd HH:mm:ss'),
      dateEnd: format(endDate.toISOString(),'yyyy-MM-dd HH:mm:ss'),
      key: ""
    };
    postCustomerMutation.mutate({
      data
    });
  };

  const columns = [
    {
      field: "stt",
      headerName: "ID",
      flex: 0.5,
    },
    // 
    {
      field: "soGoiDen",
      headerName: "Số Điện Thoại",
      flex: 1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {returnPhone(params.row.soGoiDen)}
        </Typography>
      )
    },
    {
      field: "ngayGoi",
      headerName: "Ngày Gọi",
      flex: 1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
          {formartDate(params.row.ngayGoi, 'short')}
    
        </Typography>
      )
    },
    {
      field: "trangThai",
      headerName: "Trạng Thái",
      flex: 1,
    },

  ];
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  return (
    <>
    <div className='block'>
      <div className={styles.callsboard}>
      <div className={styles.reportCall}>
        <h5 className={styles.title} style={{color:'1E0342',marginBottom:'10px'}}>
        <HomeIcon/>  <ArrowForwardIosIcon/>Cuộc Gọi Nhỡ
          </h5>
        <div className='flex dates'>
          <div>
          <CalendarMonthIcon sx={{marginRight:'5px'}}/>
            <DatePicker
             
              locale={vi}
              selected={startDate}
              onChange={(date) => {
                console.log(format(date,'yyyy-MM-dd HH:mm:ss'));
                return setStartDate(date);
              }}
             
            
            
              dateFormat="MMMM d, yyyy"
            />
          </div>
          <strong> Tới</strong>
          <CalendarMonthIcon sx={{marginRight:'5px',marginLeft:'5px'}}/>
          <div>
            <DatePicker
              locale={vi}
              selected={endDate}
              onChange={(date) => {
                return setEndDate(date);
              }}
              dateFormat="MMMM d, yyyy"
            />
          </div>
          <div >
          <Button onClick={handleAddUser}  sx={{ backgroundColor: '#A0DEFF' }}>
                Tìm Kiếm
              </Button>
              </div>
        </div>
       
      </div>
    </div>
    <Container>
          
          <Box
            width='77.9vw'
            
          >
            
              <Box style={{ width: '100%' }}
                 sx={{
                  "& .MuiDataGrid-root": {
                    border: "none",
                  },
                  "& .MuiDataGrid-cell": {
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-columnHeaders": {
                    backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
                    color: "black", // Màu trắng
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-virtualScroller": {
                    backgroundColor: "#FFF", // Màu xanh dương nhạt
                  },
                  "& .MuiDataGrid-footerContainer": {
                    backgroundColor: "#black", // Màu xanh dương
                    color: "#FFF", // Màu trắng
                    borderTop: "none",
                  },
                  "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                    color: "#2196F3 !important", // Màu đỏ
                  },
                  ".MuiDataGrid-toolbarContainer .MuiText": {
                    color: "#2196F3 !important", /* Màu văn bản */
                  },
                  '& .MuiDataGrid-cell': {
                    fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
                  },
                  '& .MuiDataGrid-columnHeader': {
                    fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
                  },
                }}
              >
                <DataGrid
                  rows={callData || []}
                  getRowId={(row) => row.id + row.ngayGoi}
                  columns={columns}
                  pageSize={5}
                  autoHeight
                  components={{ Toolbar: DataGridCustomToolbar }}
                  
                />
              </Box>
            
          </Box>
    </Container>
    </div>
    </>
  );
}

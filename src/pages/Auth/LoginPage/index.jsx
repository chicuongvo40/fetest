import React from 'react'
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { useAuth } from '~/context/AuthContext';
import {
  Card,
  Input,
  Checkbox,
  Button,
  Typography,
} from "@material-tailwind/react";
import img from "~/assets/water.jpg"
const Login = () => {
  const {login} = useAuth();
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  async function handleLogin(e) {
    e.preventDefault();
    if (!name
       ||
      !password) {
      return toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
    }
    await login.mutateAsync({ email: name, password });
  }

  return (
    <section className="m-8 flex gap-4 overflow-hidden">
      <div className="w-full lg:w-3/5 mt-24 h-full" >
        <div className="text-center">
          <Typography variant="h2" className="font-bold mb-4">Đăng Nhập</Typography>
          <Typography variant="paragraph" color="blue-gray" className="text-lg font-normal">Nhập tài khoản và password để đăng nhập.</Typography>
        </div>
        <form className="mt-8 mb-2 mx-auto w-80 max-w-screen-lg lg:w-1/2">
          <div className="mb-1 flex flex-col gap-6 pl-0 ">
            <Typography variant="small" color="blue-gray" className="-mb-3 font-medium mr-[300px] ">
              Tài Khoản
            </Typography>
            <Input
              onChange={(e) => setName(e.target.value)}
              size="lg"
              placeholder="name@mail.com"
              className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
              labelProps={{
                className: "before:content-none after:content-none",
              }}
            />
            <Typography variant="small" color="blue-gray" className="-mb-3 font-medium mr-[300px]">
              Password
            </Typography>
            <Input
              onChange={(e) => setPassword(e.target.value)}
              type="password"
              size="lg"
              placeholder="********"
              className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
              labelProps={{
                className: "before:content-none after:content-none",
              }}
            />
          </div>
          <Checkbox
            label={
              <Typography
                variant="small"
                color="gray"
                className="flex items-center justify-start font-medium"
              >
                I agree the&nbsp;
                <a
                  href="#"
                  className="font-normal text-black transition-colors hover:text-gray-900 underline"
                >
                  Terms and Conditions
                </a>
              </Typography>
            }
            containerProps={{ className: "-ml-2.5" }}
          />
          <Button className="mt-6" fullWidth onClick={handleLogin}>
            Đăng Nhập
          </Button>

        </form>

      </div>
      <div className="w-2/5 hidden lg:block" style={{height : '90vh'}}>
        <img
          src={img}
          className="h-full w-full object-cover rounded-3xl"
        />
      </div>

    </section>

  );
}

export default Login;
import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '~/context/AxiosContex';
import EditIcon from '@mui/icons-material/Edit';
import { AddCampaignAdm } from '~/components/Addition/AcceptedCustomer';
import styles from './CallReport.module.css';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
export default function AcceptCustomer() {
  const [id, setId] = useState();
  const [xstaff, setStaff] = useState([]);
  const { getcustomerrequest } = useAxios();
  const [open, setOpen] = useState(false);

  const handleOpen = (id) => {
    setId(id);
    setOpen(true);
  }
  const handleClose = () => setOpen(false);
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };
  const {
    data: staff,
    isLoading,
    refetch
  } = useQuery(
    {
      queryKey: ['staff'],
      queryFn: () => getcustomerrequest(),
    }
  );
  useEffect(() => {
    if (staff) {
      setStaff(staff);
      console.log(staff);
    }
  }, [staff]);


  return (
    <>
      {open ? (
        <AddCampaignAdm
          refetch={refetch}
          setOpenModal={setOpen}
          idBrand={id}
        />
      ) : null}


      <div className={styles.listCall}>
        <div className={styles.box__content}>
          <div className={styles.head__box}>
            <h3>Yêu Cầu từ Khác Hàng</h3>
          </div>
          <div className={styles.head_new}>
         
            <span>STT</span>
            <span>SĐT</span>
            <span>Tên</span>
            <span>TIêu Đề</span>
            <span>Accept</span>
         
          </div>
          {
            xstaff.length < 1 ?? (<p>Không có yêu cầu từ khách hàng</p>)
          }
          {xstaff &&
            xstaff.map((htr, index) => (
                <div
                  key={index}
                  className={styles.content_new}
                >
                  <span>{index + 1}</span>
                  <span>{htr.customerId.phoneNumber}</span>
                  <span >
                    {htr.customerId.name}
                  </span>
                  <span >
                    {htr.title}
                  </span>

                  <button  onClick={() => handleOpen(htr.id)}>
                            <AddCircleOutlineIcon />
                            </button>
                </div>
              ))}

        </div>
      </div>



      {/* <div className="mt-12 mb-8   ">
        <Card>
          <CardHeader variant="gradient" color="gray" className="mb-8 p-6 pr-40 pl-40 justify-start">
            <Typography variant="h6" color="white" sx={{ marginRight: '100px' }}>
              Yêu Cầu Khách Hàng
            </Typography>
          </CardHeader>
          <CardBody className="overflow-x-scroll px-0 pt-0 pb-2">
            <table className="w-full min-w-[640px] table-auto">
              <thead>
                <tr>
                  {["STT", "SĐT", "Tên", "TIêu Đề", "Accept"].map(
                    (el) => (
                      <th
                        key={el}
                        className="border-b border-blue-gray-50 py-3 px-5 text-left"
                      >
                        <Typography
                          variant="small"
                          className="text-[11px] font-bold uppercase text-blue-gray-400"
                        >
                          {el}
                        </Typography>
                      </th>
                    )
                  )}
                </tr>
              </thead>
              <tbody>
                {   xstaff.length > 0 ? 
               xstaff.map((key, index)=> {
                  const className = `py-3 px-5 ${key === xstaff[xstaff.length - 1] ? "" : "border-b border-blue-gray-50"
                    }`;
                    return (
                      <tr key={key.id}>
                        <td className={className}>
                          <div className="flex items-center gap-4">
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-bold"
                            >
                              {index + 1}
                            </Typography>
                          </div>
                        </td>
                        <td className={className}>
                          {key.customerId.phoneNumber}
                        </td>
                        <td className={className}>
                          <Typography
                            variant="small"
                            className="text-xs font-medium text-blue-gray-600"
                          >
                            {key.customerId.name}
                          </Typography>
                        </td>
                        <td className={className}>
                          <div className="w-10/12">
                            <Typography
                              variant="small"
                              className="mb-1 block text-xs font-medium text-blue-gray-600"
                            >
                              {key.title}
                            </Typography>
                          </div>
                        </td>
                        <td className={className}>
                        <button  onClick={() => handleOpen(key.id)}>
                            <AddCircleOutlineIcon />
                            </button>
                        </td>
                      </tr>
                    );
                    
                }) : (<p>Không có gì</p>)
                }
              </tbody>
            </table>
          </CardBody>
        </Card>
      </div> */}

    </>
  );
}
import styles from './CallReport.module.css';
import { AiFillEdit } from 'react-icons/ai';
import { GrView } from 'react-icons/gr';
import DatePicker from 'react-datepicker';
import vi from 'date-fns/locale/vi';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { FcCallback } from 'react-icons/fc';
import { Loading } from '~/components/LoadingPage/Loading';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '~/context/AxiosContex';
import { useAuth } from '~/context/AuthContext';
import { toast } from 'react-hot-toast';
import { AiOutlinePlus, AiFillFileExcel } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import ChartCircleCall from '~/components/Charts/ChartCircleCall2';
export default function KpiTicket() {
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 1 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(
    new Date(new Date().getTime() + 1* 60 * 60 * 1000),
  );
  const [xstaff, setStaff] = useState([]);
  const [selectMonth, setSelectMonth] = useState(new Date());
  // const navigate = useNavigate();
  const { getticketKPI } = useAxios();
  const {
    error,
    data: staff,
    isLoading
  } = useQuery(
    {
      queryKey: ['staff', startDate, endDate],
      queryFn: () => getticketKPI({
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
      }),
    }
  );
  useEffect(() => {
    if (staff) {
      setStaff(staff);
      console.log(staff);
    }
  }, [staff]);
  return (

    <>
    <div className={styles.dates}>
          <div>
            <DatePicker
              locale={vi}
              selected={startDate}
              onChange={(date) => {
                if (endDate - date > 31 * 24 * 60 * 60 * 1000) {
                  toast('Chỉ xử lý được trong khoảng 30 ngày', {
                    icon: '👏',
                    style: {
                      borderRadius: '10px',
                      background: '#333',
                      color: '#fff',
                    },
                  });
                  return;
                }
                return setStartDate(date);
              }}
            />
          </div>
          <strong> Tới</strong>
          <div>
            <DatePicker
              locale={vi}
              selected={endDate}
              onChange={(date) => {
                if (date - startDate > 31 * 24 * 60 * 60 * 1000) {
                  toast('Chỉ xử lý được trong khoảng 30 ngày', {
                    icon: '👏',
                    style: {
                      borderRadius: '10px',
                      background: '#333',
                      color: '#fff',
                    },
                  });
                  return;
                }
                return setEndDate(date);
              }}
            />
          </div>
        </div>
      <div className={styles.head__box}>
        <h3>Quản Lý Ticket KPI</h3>
      </div>
      {xstaff.length > 0 ? (
        <>
          <div>
            <ChartCircleCall htrCall={xstaff} />
          </div>
          <div className={styles.listCall}>
            {isLoading ? (
              <div className={styles.loading__tb}>
                <Loading size='90' color='#fc3b56' />
              </div>
            ) : (
              <>
                <div className={styles.head_new}>
                  <span>STT</span>
                  <span>Loại Ticket</span>
                  <span>Thời gian hoàn thành</span>
                  <span>Tổng số Ticket</span>
                  <span>Đúng Hạn</span>
                  <span>Quá Hạn</span>
                </div>
                {xstaff.map((elm, index) => (
                  <div key={index} className={styles.content_new}>
                    <span>{index + 1}</span>
                    <span>{elm.name}</span>
                    <span>{elm.time}</span>
                    <span>{elm.fail + elm.success}</span>
                    <span>{elm.success}</span>
                    <span>{elm.fail}</span>
                  </div>
                ))}
              </>
            )}
          </div>
        </>
      ) : (
        <p>Không có gì</p>
      )}
    </>

  );
}
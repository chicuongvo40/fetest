import styles from './CallReport.module.css';
import { AiFillEdit } from 'react-icons/ai';
import { GrView } from 'react-icons/gr';
import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { FcCallback } from 'react-icons/fc';
import { Loading } from '~/components/LoadingPage/Loading';
import ChartCircleCall from '~/components/Charts/ChartCircleCall';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '~/context/AxiosContex';
import { useAuth } from '~/context/AuthContext';
import { toast } from 'react-hot-toast';
import { AiOutlinePlus, AiFillFileExcel } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import { AddCampaignAdm } from '~/components/Addition/AddStaffBranch';
import { EditCampaignAdm } from '~/components/Edition/EditPass';
import {
  Modal,
  Typography,
  Button,
  Box,
  Icon,
  Table,
  styled,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  IconButton
} from "@mui/material";
export default function CustomerReport() {
  const { user } = useAuth();
  const [open, setOpen] = useState(false);
  const [modal, setModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [addStaff, setAddStaff] = useState(false);
  const handleOpen = (op) => {
    setModal(op);
    setOpen(true);
  }
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };
  const [idBrand, setIdBrand] = useState();
  const handleClose = () => setOpen(false);
  const [xstaff, setStaff] = useState([]);
  const { getStaff } =
    useAxios();
  const {
    data: staff,
    isLoading,
    refetch
  } = useQuery(
    {
      queryKey: ['staff'],
      queryFn: () => getStaff(Object.values(user)[4]),
    }
  );
  function handleEdit(id) {
    setIdBrand(id);
    setEditModal(true);
  }
  useEffect(() => {
    if (staff) {
      setStaff(staff);
      console.log(staff);
    }
  }, [staff]);
  return (
    <>
      {addStaff && (
        <AddCampaignAdm
          refetch={refetch}
          setOpenModal={setAddStaff}
        />
      )}
  
      {editModal && (
        <EditCampaignAdm
          refetch={refetch}
          idBrand={idBrand}
          setEditModal={setEditModal}
        />
      )}
  
      <div>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Thông Tin
            </Typography>
            <Box display='flex'>
              <Box sx={{ marginRight: '20px' }}>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Họ và Tên
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Tài Khoản
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Mật Khẩu
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Trạng Thái
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Giới Tính
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Address
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Ngày Sinh
                </Typography>
              </Box>
              <Box>
                <Typography id="modal-modal-title" variant="h6" component="h2"></Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.lastName} {modal.firstName}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.userName}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.password}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.status == 1 && 'Online'}
                  {modal.status == 2 && 'Offline'}
                  {modal.status == 3 && 'Rảnh'}
                  {modal.status == 4 && 'Bận'}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.gender == 1 && 'Nam'}
                  {modal.gender == 2 && 'Nữ'}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.address}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  : {modal.dayOfBirth}
                </Typography>
              </Box>
            </Box>
            <Box sx={{ mt: 2 }}>
              <Button sx={{ color: 'red' }} onClick={handleClose}>Đóng</Button>
            </Box>
          </Box>
        </Modal>
      </div>
  
      <div className={styles.head__box}>
        <h3>Quản lý nhân viên: {staff != null ? staff.length : 0 }</h3>
      </div>
  
      <div className={styles.listCall}>
        {isLoading ? (
          <div className={styles.loading__tb}>
            <Loading
              size='90'
              color='#fc3b56'
            />
          </div>
        ) : (
          <>
            {xstaff.length > 0 ? (
              <>
                <div className={styles.head_new}>
                  <span>STT</span>
                  <span>Tên nhân viên</span>
                  <span>Trạng thái</span>
                  <span>TK Đăng Nhập</span>
                  <span>Mật Khẩu</span>
                  <span>Địa chỉ</span>
                  <span>Sửa</span>
                  <span>Xem</span>
                  <Button sx={{ color: 'blue' }} onClick={() => setAddStaff(true)}>  <span>Thêm</span></Button>
                </div>
                {xstaff.map((elm, index) => (
                  <div key={index} className={styles.content_new}>
                    <span>{elm.id}</span>
                    <span>{elm.firstName + ' ' + elm.lastName}</span>
                    <span>
                      {elm.status == 1 && 'Online'}
                      {elm.status == 2 && 'Offline'}
                      {elm.status == 3 && 'Rảnh'}
                      {elm.status == 4 && 'Bận'}
                    </span>
                    <span>{elm.userName}</span>
                    <span>{elm.password}</span>
                    <span>{elm.address}</span>
                    <span>
                      <AiFillEdit
                        onClick={() => handleEdit(elm.id)}
                        className={styles.editBtn}
                        style={{
                          cursor: 'pointer',
                          color: '#1775f1',
                          fontSize: '22px',
                        }}
                      />
                    </span>
                    <span>
                      <GrView
                        className={styles.editBtn}
                        onClick={() => handleOpen(elm)}
                        style={{
                          cursor: 'pointer',
                          color: '#1775f1',
                          fontSize: '22px',
                        }}
                      />
                    </span>
                  </div>
                ))}
              </>
            ) : (
              <p> Không có gì cả</p>
            )}
          </>
        )}
      </div>
    </>
  );
}
import React from 'react';
import {
  Box,
  Card,
  CardContent,
  Button,
  Typography,
  Grid,
  Menu, MenuItem
} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import { useNavigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect } from 'react';
//Context
import { useAxios } from '~/context/AxiosContex';
//Utils
import { formartDate } from '~/utils/functions';
//Components
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import PhoneIcon from '@mui/icons-material/Phone';
import KeyIcon from '@mui/icons-material/Key';
import FilterListIcon from '@mui/icons-material/FilterList';
import SearchIcon from '@mui/icons-material/Search';
import DebouncedInput from "~/components/Material/DebouncedInput/DebouncedInput";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { format } from 'date-fns';
import AddCampaignAdm from '~/components/Addition/AddStaff';
import AddCampaignAdm1 from '~/components/Material/Confirm';
import { useAuth } from '~/context/AuthContext';
export default function StaffManager() {
  const { user } = useAuth();
  const [openEditModal, setEditOpenModal] = useState(false);
  const [openEditModal1, setEditOpenModal1] = useState(false);
  //Axios
  const { getTicketByStatus } = useAxios();
  //Usestate
  const [ticketData, setTicketData] = useState([]);
  const [ticketData1, setTicketData1] = useState([]);
  const [ticketData2, setTicketData2] = useState([]);
  const [ticketData3, setTicketData3] = useState([]);
  const [id, setId] = useState("");
  const [email, setEmail] = useState("");
  const [date, setDate] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const [search, setSearch] = useState(null);
  const handleChange = date => {
    setSelectedDate(date);
  };

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  //Navigation
  const navigate = useNavigate();
  //GetTicket
  const {
    data: ticket,
    refetch
  } = useQuery(
    {
      queryKey: ['ticket'],
      queryFn: () => getTicketByStatus(Object.values(user)[4]),
    }
  );
  function handleOnClick(id) {
    console.log('Co Nhan nha');
    setEditOpenModal(true);
    setId(id);
  }
  function handleOnClick1(id,op) {
    setEmail(op);
    setId(id);
    console.log('Co Nhan nha');
    setEditOpenModal1(true);
   
  }
  useEffect(() => {
    if (ticket) {
      console.log(ticket.statusOne);
      setTicketData(ticket.statusOne);
      setTicketData1(ticket.statusTwo);
      setTicketData2(ticket.statusThree);
      setTicketData3(ticket.statusFour);
    }

  }, [ticket]);

  // //Api link
  // const { getTicketName } = useAxios();
  // //Usestate
  // const [customerData, setCustomerData] = useState([]);
  // const [id, setId] = useState("");
  // const [openEditModal, setEditOpenModal] = useState(false);
  // // get customers
  // const {
  //   data: customers,
  //   isLoading,
  //   refetch,
  // } = 
  // useQuery(
  //   {
  //     queryKey: ['customers'],
  //     queryFn: async () => await getTicketName(),
  //   }
  // );
  // function handleOnClick(id){
  //   console.log('Co Nhan nha');
  //   setEditOpenModal(true);
  //   setId(id);
  // }
  // useEffect(() => {
  //   if (customers) {
  //     setCustomerData(customers);
  //     console.log(customers);
  //   }
  // }, [customers]);
  // const columns = [
  //   {
  //     field: "id",
  //     headerName: "ID",
  //     flex:0.1,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {params.row.id}
  //     </Typography>
  //     )
  //   },
  //   {
  //     field: "ticketStatusId",
  //     headerName: "Trạng Thái",
  //     flex:0.1,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {params.row.ticketStatusId}
  //     </Typography>
  //     )
  //   },
  //   {
  //     field: "title",
  //     headerName: "Tiêu Đề",
  //     flex:0.1,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //     a
  //     </Typography>
  //     )
  //   },
  //   {
  //     field: "callId",
  //     headerName: "Mã Cuộc Gọi",
  //     flex:0.2,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {params.row.callId}
  //     </Typography>
  //     )
  //   },  
  //   {
  //     field: "customerId",
  //     headerName: "Khách Hàng",
  //     flex:0.2,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {params.row.customerId}
  //     </Typography>
  //     )
  //   },
  //   {
  //     field: "ticketTypeId",
  //     headerName: "Loại",
  //     flex:0.2,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {/* {formartDate(params.row.dayOfBirth, 'short')} */}
  //       {params.row.ticketTypeId}
  //     </Typography>
  //     )
  //   },
  //   {
  //     field: "levelId",
  //     headerName: "Level",
  //     flex:0.1,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {/* {formartDate(params.row.dateCreated, 'short')} */}
  //       {params.row.levelId}
  //     </Typography>
  //     )
  //   }, 
  //   {
  //     field: "createdBy",
  //     headerName: "Ngày Tạo",
  //     flex:0.2,
  //     renderCell: (params) => (
  //       <Typography variant="body1" fontSize="13px">
  //       {params.row.createdBy}
  //     </Typography>
  //     )
  //   },
  //   {
  //     field: "userId",
  //     headerName: "Nhân Viên",
  //     flex:0.1,
  //     renderCell: (params) => (
  //       <Button onClick={
  //         () => {
  //           handleOnClick(params.row.id)
  //         }
  //       }>
  //       <Typography variant="body1" fontSize="13px" >
  //       {/* {params.row.createdBy} */}
  //     </Typography>
  //     </Button>
  //     )
  //   },
  //   {
  //     headerName: "DETAIL",
  //     field: "detail",
  //     flex:0.1,
  //     headerAlign: 'center',
  //     align: 'center',
  //     renderCell: (params) => (
  //       <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color:'red', width:'100vw'}}> 
  //           <VisibilityIcon/>
  //       </Box>
  //     )
  //   },
  // ];
  return (
    //     <>
    //       {openEditModal ? (
    //         <AddCampaignAdm
    //           idBrand={id}
    //           refetch={refetch}
    //           setEditModal={setEditOpenModal}
    //         />
    //       ) : null}
    //       <Box>
    //     <Box
    //       mt="0px"
    //       height="75vh"
    //       width='74vw'
    //       sx={{
    //         "& .MuiDataGrid-root": {
    //           border: "none",
    //         },
    //         "& .MuiDataGrid-cell": {
    //           borderBottom: "none",
    //         },
    //         "& .MuiDataGrid-columnHeaders": {
    //           backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
    //           color: "black", // Màu trắng
    //           borderBottom: "none",
    //         },
    //         "& .MuiDataGrid-virtualScroller": {
    //           backgroundColor: "#FFF", // Màu xanh dương nhạt
    //         },
    //         "& .MuiDataGrid-footerContainer": {
    //           backgroundColor: "#black", // Màu xanh dương
    //           color: "#FFF", // Màu trắng
    //           borderTop: "none",
    //         },
    //         "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
    //           color: "#2196F3 !important", // Màu đỏ
    //         },
    //         ".MuiDataGrid-toolbarContainer .MuiText": {
    //           color: "#2196F3 !important", /* Màu văn bản */
    //         },
    //         '& .MuiDataGrid-cell': {
    //           fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
    //         },
    //         '& .MuiDataGrid-columnHeader': {
    //           fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
    //         },
    //       }}
    //     >
    //       <DataGrid
    //        loading={isLoading || !customerData}
    //        rows={customerData || []}
    //        getRowId={(row) => row.id}
    //        columns={columns}
    //        components={{ Toolbar: DataGridCustomToolbar }}
    // />
    //     </Box>
    //   </Box>



    //     </>
    <>
      {openEditModal ? (
        <AddCampaignAdm
          idBrand={id}
          refetch={refetch}
          setEditModal={setEditOpenModal}
        />
      ) : null}
       {openEditModal1 ? (
        <AddCampaignAdm1
          email={email}
          idBrand={id}
          refetch={refetch}
          setEditModal={setEditOpenModal1}
        />
      ) : null}
      {
        ticket &&
        <Box width='80vw'>
          <Box display='block'>
            <div className="w-full flex items-center gap-1">
              <SearchIcon />
              <DebouncedInput
                value={search ?? ""}
                onChange={(value) => setSearch(value)}
                className="p-2 bg-transparent outline-none  w-1/5  border-blue-gray-900"
                placeholder="Search by phonenumber"
              />
              <CalendarMonthIcon sx={{ marginLeft: 5 }} />
              <DatePicker
                selected={selectedDate}
                onChange={handleChange}
                dateFormat="yyyy-MM-dd"
                placeholderText="Select a date"
              />
              {selectedDate && <Button onClick={() => setSelectedDate(null)}>Clear</Button>}
            </div>
          </Box>

          <Box mt="1.5rem" gridTemplateColumns="repeat(4,1fr)" height='600px' display='block'>
            <Grid container spacing={2}>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: 'green'
                    }}>Tiếp Nhận ({ticketData.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData && ticketData.length > 0 && ticketData.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {

                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between mb-2'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                    {item.title}
                                  </Typography>
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>

                                <Box display={'flex'}  justifyContent="space-between" sx={{ marginTop: '5px' }}>
                                  <Button variant="contained" sx={{ bgcolor: '#4CAF50', color: '#FFF', mr: 1, p: '4px 8px' }} onClick={() => { handleOnClick(item.id) }}>
                                    <AddIcon sx={{ fontSize: '14px' }} />
                                  </Button>
                                  <Button onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} variant="contained" sx={{ bgcolor: '#2196F3', color: '#FFF', p: '4px 8px' }}>
                                    <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                      Detail
                                    </Typography>
                                  </Button>
                                </Box>
                              </CardContent>

                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: '#F9C46B'
                    }}>Đang Xử Lý ({ticketData1.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData1 && ticketData1.length > 0 && ticketData1.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {

                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                    {item.title}
                                  </Typography>
                                 
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                                <Box display={'flex'}  justifyContent="space-between" sx={{ marginTop: '5px' }}>
                                
                                  <Button onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} variant="contained" sx={{ bgcolor: '#2196F3', color: '#FFF', p: '4px 8px' }}>
                                    <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                      Detail
                                    </Typography>
                                  </Button>
                                </Box>
                              </CardContent>
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: '#2A6FDB'
                    }}>Đã Xử Lý ({ticketData2.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData2 && ticketData2.length > 0 && ticketData2.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {

                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                    {item.title}
                                  </Typography>
                               
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                                <Box display={'flex'}  justifyContent="space-between" sx={{ marginTop: '5px' }}>
                                  <Button variant="contained" sx={{ bgcolor: '#4CAF50', color: '#FFF', mr: 1, p: '4px 8px' }} onClick={() => { handleOnClick1(item.id,item.customerId.email) }}>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                      Kết Thúc
                                    </Typography>
                                  </Button>
                                  <Button onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)} variant="contained" sx={{ bgcolor: '#2196F3', color: '#FFF', p: '4px 8px' }}>
                                    <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                      Detail
                                    </Typography>
                                  </Button>
                                </Box>
                              </CardContent>
                              
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

              <Grid item xs={3}>
                <Box sx={{
                  bgcolor: '#F6F5F2',
                  ml: 2,
                  borderRadius: '6px',
                  height: 'fit-content',
                  maxHeight: '600px'
                }}>
                  <Box sx={{
                    height: '50px',
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                  }}>
                    <Typography sx={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                      color: '#503A65'
                    }}>Kết Thúc ({ticketData3.length})</Typography>
                    <FilterListIcon sx={{}} />
                  </Box>
                  <Box sx={{
                    '&::-webkit-scrollbar:hover': {
                      display: 'display !important' // Override display property on hover
                    },
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 1,
                    overflowX: 'hidden',
                    overflowY: 'auto', // Change 'overflow' to 'overflowY' for vertical scrolling
                    maxHeight: '520px'
                  }}>
                    <div>
                      {
                        ticketData3 && ticketData3.length > 0 && ticketData3.filter((val) => {
                          if (selectedDate === null && search === null) {
                            return true;
                          }
                          if (selectedDate !== null && search !== null) {

                            console.log(format(selectedDate, 'dd/MM/yyyy'));
                            return format(val.createdDate, 'dd/MM/yyyy') === format(selectedDate, 'dd/MM/yyyy') && val.customerId.phoneNumber.includes(search);
                          }
                          if (selectedDate !== null && search === null) {
                            return format(val.createdDate, 'dd/MM/yyyy') === selectedDate.toLocaleDateString();
                          }
                          if (selectedDate === null && search !== null) {
                            return val.customerId.phoneNumber.includes(search);
                          }
                          return false;
                        })
                          .map(item => (
                            <Card
                              key={item.id}
                              sx={{
                                cursor: 'pointer',
                                boxShadow: '0 1px 1px rgba(0,0,0,0.2)',
                                overflow: 'unset',
                                marginBottom: '10px'
                              }}
                            >
                              <CardContent
                                sx={{
                                  p: 1.5,
                                  '&:last-child': { p: 1.5 }
                                }}
                              >
                                <p className='flex justify-between'>
                                  <Typography sx={{ fontSize: '12px', fontWeight: 'bold' }}>
                                    {item.title}
                                  </Typography>
                                
                                </p>
                                <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                  <KeyIcon fontSize="small" /> TK32132{item.id}
                                </Typography>
                                <div className="flex justify-between">
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <PhoneIcon fontSize="small" /> {item.customerId.phoneNumber}
                                  </Typography>
                                  <Typography sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                    <CalendarMonthIcon fontSize="small" /> {formartDate(item.createdDate, 'short')}
                                  </Typography>
                                </div>
                                <Box display={'flex'}  justifyContent="space-between" sx={{ marginTop: '5px' }}>
                                  <Button variant="contained" sx={{ bgcolor: '#2196F3', color: '#FFF', p: '4px 8px' }}>
                                    <Typography onClick={() => navigate(`/${item.id}/${item.customerId.id}/ticket`)}  sx={{ fontSize: '10px', fontWeight: 'bold' }}>
                                      Detail
                                    </Typography>
                                  </Button>
                                </Box>
                                
                              </CardContent>
                            </Card>
                          ))}
                    </div>

                  </Box>

                </Box>
              </Grid>

            </Grid>

          </Box>
        </Box>
      }
    </>
  );
}

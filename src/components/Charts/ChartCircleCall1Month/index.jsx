import { useEffect, memo } from 'react';
import * as echarts from 'echarts';


export default memo(function ChartCircleCall1Month({ htrCall }) {
  useEffect(() => {
    console.log(htrCall);
    if (htrCall) {
      const chartDom = document.getElementById('main4');
      const myChart = echarts.init(chartDom);
      let option;

      option = {
        tooltip: {
          trigger: 'item',
        },
        legend: {
          top: '5%',
          left: 'center',
        },
        series: [
          {
            name: 'Thông tin',
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            itemStyle: {
              borderRadius: 10,
              borderColor: '#fff',
              borderWidth: 2,
            },
            label: {
              show: false,
              position: 'center',
            },
            emphasis: {
              label: {
                show: true,
                fontSize: 20,
                fontWeight: 'bold',
              },
            },
            labelLine: {
              show: false,
            },
            data: [
              { value: htrCall?.a, name: 'Tiếp Nhận' },
              { value: htrCall?.b, name: 'Đang Xử Lý' },
              { value: htrCall?.c, name: 'Đã Xử Lý' },
              { value: htrCall?.d, name: 'Kết thúc' },
            ],
          },
        ],
      };

      option && myChart.setOption(option);
    }
  }, [htrCall]);
  return (
    <div
      style={{ height: '400px', width: '100%' }}
      id='main4'
    ></div>
  );
})

// Styles
import styles from './AddCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '~/components/Material/Input';
import { Button } from '~/components/Material/Button';
// Context
import { useAxios } from '~/context/AxiosContex';
import { useQuery } from '@tanstack/react-query';
// Modules
import { motion } from 'framer-motion';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { useRef } from 'react';

export function AddCampaignAdm({ setOpenModal, refetch,idBrand }) {
  const [branchName, setBrandName,] = useState('');
  const { updateTicketAcceptedCustomer,postMail,getTicketIdById } = useAxios();
  const {
    data: getBrandId,
  } = useQuery(
    {
      queryKey: ['getBrandId'],
      queryFn: async () => await getTicketIdById(idBrand),
    }
  );
  const postMailMutation = useMutation({
    mutationFn: (data) => {
      return postMail(data).then((res) => {
        console.log(res);
        return res;
      });
    },
  });
  //* Hàm tạo Ticket
  const handlePostMail = (e) => {
  console.log(email);
  const data = {
    subject: "Ticket đã được xác nhận",
    body: "Ticket đã được chấp nhận, Chúng tôi sẽ gửi người đến hỗ trợ bạn trong thời gian sớm nhất. Xin cảm ơn",
    toAddresses:[getBrandId.customerId.email]
  };
  postMailMutation.mutate({
    data
  });
};



//  id: getBrandId.id,
//     branchId: getBrandId.branchId,
//     note: getBrandId.note,
//     customerId: getBrandId.customerId,
//     ticketTypeId: getBrandId.ticketTypeId,
//     levelId: getBrandId.levelId,
//     ticketStatusId: 1,
//     createdDate: getBrandId.createdDate,
//     createdBy: 'staff',
//     assignedUserId:  1,
//     code: getBrandId.code,
//     title: getBrandId.title,
const updateBrandMutation = useMutation({
  mutationFn: (data) => {
    console.log(data);
    return updateTicketAcceptedCustomer(data).then((res) => {
      return res;
    });
  },
  onSuccess: (data) => { 
    handlePostMail();
    console.log(data);
    if (data == '204') {
      toast('Update thành công', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      refetch();
      setOpenModal(false);
    }
  },
});
function handleUpdateBrand(e) {
  e.preventDefault();
  console.log(getBrandId);
  const dataPatch = {
     id: getBrandId.id,
    branchId: getBrandId.branchId,
    note: getBrandId.note,
    customerId: getBrandId.customerId,
    ticketTypeId: getBrandId.ticketTypeId,
    levelId: getBrandId.levelId,
    ticketStatusId: 1,
    createdDate: getBrandId.createdDate,
    createdBy: 'staff',
    assignedUserId:  1,
    code: getBrandId.code,
    title: getBrandId.title,
  };
  updateBrandMutation.mutate({ data: dataPatch });
}
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>Chấp Nhận Yêu Cầu</h6>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
        
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setOpenModal(false);
              }}
            >
              HỦY
            </Button>
            <Button
             onClick={handleUpdateBrand}
              className={styles.addbtn}
            >
              Chấp Nhận
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}

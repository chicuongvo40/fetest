// Styles
import styles from './AddCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '~/components/Material/Input';
import { Button } from '~/components/Material/Button';
// Context
import { useAxios } from '~/context/AxiosContex';
// Modules
import { useAuth } from '~/context/AuthContext';
import { motion } from 'framer-motion';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { useRef } from 'react';
import { format } from 'date-fns';
export function AddCampaignAdm({ setOpenModal, refetch }) {
  const [lastname, setlastname] = useState('');
  const [firstName, setfirstName] = useState('');
  const [userName, setuserName] = useState('');
  const [password, setpassword] = useState('');
  const [gender, setgender] = useState('');
  const [address, setaddress] = useState('');
  const [dayOfBirth, setdayOfBirth] = useState('');
  const { postUser } = useAxios();
  const { user } = useAuth();
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postUser(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data == '' ) {
        refetch();
        toast('Tạo thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        setOpenModal(false);
      } else {
        toast('Có lỗi trong quá trình tạo CustomerLevel', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });
  //* Hàm tạo khách mới
  const handleAddBrand = (e) => {
    e.preventDefault();
    if (
      !address 
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      branchId: Object.values(user)[4],
      roleId: 1,
      lastName: lastname,
      firstName: firstName,
      userName: userName ,
      password: password ,
      status: 3,
      gender: 1,
      address: address,
      dayOfBirth: dayOfBirth,
      createdDate: format(Date.now(), 'yyyy-MM-dd')
    };
    postCustomerMutation.mutate({
      data
    });
  };
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6> Thêm nhân viên</h6>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
        
          <div className={styles.field}>
            <label>Họ</label>
            <Input
              value={lastname }
              onChange={(e) => setlastname(e.target.value)}
              type='text'
            />
            <label>Tên</label>
            <Input
              value={firstName}
              onChange={(e) => setfirstName(e.target.value)}
              type='text'
            />
            <label>Tài khoản</label>
            <Input
              value={userName}
              onChange={(e) => setuserName(e.target.value)}
              type='text'
            />
            <label>Password</label>
            <Input
              value={password}
              onChange={(e) => setpassword(e.target.value)}
              type='text'
            />
            <label>Địa chỉ</label>
            <Input
              value={address}
              onChange={(e) => setaddress(e.target.value)}
              type='text'
            />
            <label>Giới tính</label>
              <select  onChange={(e) => setgender(e.target.value)}>
                <option value='1'>Nam</option>
                <option value='2'>Nữ</option>
              </select>
              <label>Ngày Sinh</label>
              <Input
                onChange={(e) => setdayOfBirth(e.target.value)}
                type='date'
              />
          </div>
         
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setOpenModal(false);
              }}
            >
              Hủy
            </Button>
            <Button
              onClick={handleAddBrand}
              className={styles.addbtn}
            >
              Thêm Nhân Viên
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}
